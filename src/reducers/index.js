import { combineReducers } from 'redux';
import {reducer as form} from 'redux-form';
import postReducer from './postReducer';
const rootReducer = combineReducers({
   form,
   posts:postReducer
});
export default rootReducer;