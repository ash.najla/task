import {FETCH_WEATHER,FETCH_WEATHER_SUCCESS} from '../actions/types';

const INITIAL_STATE = {
 
  weatherlist:{ info: {}, error: null, loading: true},
  authenticated: false,

};


export default function (state = INITIAL_STATE, action) {
  switch (action.type) {

    case FETCH_WEATHER:
      return {
        ...state, weatherlist: {info:{}, error: null, loading: true  }
      };

    case FETCH_WEATHER_SUCCESS:
      return { ...state, weatherlist: { info: action.payload.data, error: null, loading: false } };

    default:
      return state;
  }
}

