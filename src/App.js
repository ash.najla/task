import React, { Component } from "react";
import { Jumbotron, Button } from 'reactstrap';
import { connect } from 'react-redux';
import * as actions from './actions/index';



class App extends Component {


  getInfo(){
    this.props.getWeatherInfo()
  }

  render() {
    const {info, error, loading} = this.props.weatherlist
    return (
      <div>
      <Jumbotron>
        <h1 className="display-4">Weather Information!!</h1>
        <hr className="my-2" />
          <Button className="my-2" color="info" onClick={() => this.getInfo()}>Get Info</Button>
          <hr className="my-2" />
          {!loading?
          <div>
          <p>Location:   <b>{info.name}</b></p>
          <p>Temperature:   <b>{info.main.temp}</b></p>
          <p>Humidity:  <b>{info.main.humidity}</b></p>
          <p>Condition: <b>{info.weather[0].description}</b></p>
          </div>
          :null}
      </Jumbotron>
    </div>
    );
  }
}

function mapStateToProps(state) {
  return {
      weatherlist:state.posts.weatherlist
     
  }
} 

export default connect(mapStateToProps,actions)(App);